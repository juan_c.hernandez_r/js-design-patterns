function Employee(name, salary) {
  this.name = name;
  this.salary = salary;
}

Employee.prototype = {
  getSalary: function () {
    return this.salary;
  },

  setSalary: function (sal) {
    this.salary = sal;
  },

  accept: function (visitorFunction) {
    visitorFunction(this);
  },
};

/////////////////////////////////////

const camilodev = new Employee("CamiloDev", 1000);
console.log(camilodev.getSalary());

function ExtraSalary(emp) {
  emp.setSalary(emp.getSalary() * 2);
}

camilodev.accept(ExtraSalary);
console.log(camilodev.getSalary());
